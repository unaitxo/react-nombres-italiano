import { combineReducers } from 'redux';
import NombresReducer from './NombresReducer';
import { i18nReducer } from 'react-redux-i18n';

export default combineReducers({
  nombres: NombresReducer,
  i18n: i18nReducer
});