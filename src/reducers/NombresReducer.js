import {
  DATA_ENTERED,
  PADRE_CHANGED,
  MADRE_CHANGED,
  CHICOS_FETCH_SUCCESS,
  CHICAS_FETCH_SUCCESS,
  LOADING_NOMBRES,
  RANDOM_CHICO,
  RANDOM_CHICA,
  NOMBRES_INCOMPLETE,
  CHICOS_UPDATED,
  CHICAS_UPDATED,
  ORIGINAL_NAMES,
  SET_MODAL,
  UPDATE_LISTA
} from '../actions/types';

const INITIAL_STATE = {
  padre: '',
  madre: '',
  error: '',
  loading: false,
  chicos: {},
  chicas: {},
  randomChico: '',
  randomChica: '',
  modal: false,
  lista: []
};

export default (state = INITIAL_STATE, action) => {
  //console.log(action);
  switch (action.type) {
  	case PADRE_CHANGED:
      return { ...state, padre: action.payload };
    case MADRE_CHANGED:
      return { ...state, madre: action.payload };
    case SET_MODAL:
      return { ...state, modal: action.payload };
    case UPDATE_LISTA:
      return { ...state, lista: action.payload };  
    case DATA_ENTERED:
      return INITIAL_STATE;  
    case LOADING_NOMBRES:
      return { ...state, loading: true, error: '' };  
    case CHICOS_FETCH_SUCCESS:
      return { ...state, chicos: action.payload, loading: false,};
    case CHICAS_FETCH_SUCCESS:
      return { ...state, chicas: action.payload};
    case RANDOM_CHICO:
      return { ...state, randomChico: action.payload}; 
    case RANDOM_CHICA:
      return { ...state, randomChica: action.payload}; 
    case ORIGINAL_NAMES:
      return { ...state, randomChica: action.payload, randomChico: action.payload2}; 
    case NOMBRES_INCOMPLETE:
      return { ...state, error: action.payload }; 
    case CHICOS_UPDATED:
      return state;
    case CHICAS_UPDATED:
      return state;
    default:
      return state;
  }
};