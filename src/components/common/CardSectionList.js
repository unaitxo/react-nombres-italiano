import React from 'react';
import { View } from 'react-native';

const CardSectionList = (props) => {
  return (
    <View style={[styles.containerStyle, props.style]}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    borderWidth: 0,
    padding: 5,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    paddingBottom: 20
  }
};

export { CardSectionList };