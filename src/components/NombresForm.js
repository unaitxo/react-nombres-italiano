import React, { Component } from "react";
import {
  Text,
  Image,
  View,
  Dimensions,
  Linking,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import {
  padreChanged,
  madreChanged,
  getChicos,
  getChicas,
  nombresIncomplete,
  updateChicos,
  updateChicas,
  setModal,
  updateLista
} from "../actions";
import {
  Card,
  CardList,
  CardSection,
  CardSectionList,
  Input,
  Button,
  Spinner
} from "./common";
import firebase from "firebase";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner
} from "react-native-admob";
import { I18n } from "react-redux-i18n";
import Modal from "react-native-root-modal";
import axios from "axios";
import DeviceInfo from "react-native-device-info";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;
var error = "";
var version = "it";
var locale = DeviceInfo.getDeviceLocale().split("-");
locale = locale[0];

class NombresForm extends Component {
  componentWillMount() {
    axios
      .get("http://quesmejor.com/nombres96b505e9", { timeout: 10000 })
      .then(response => {
        this.props.updateLista(response.data);
      })
      .catch(error => {
        console.log(error);
        this.props.updateLista([]);
      });
  }

  setModal(value) {
    this.props.setModal(value);
  }

  onPadreChange(text) {
    this.props.padreChanged(text);
  }

  onMadreChange(text) {
    this.props.madreChanged(text);
  }

  onButtonLink(url) {
    Linking.openURL(url);
  }

  onButtonPress() {
    const { padre, madre } = this.props;

    if (padre != "" && madre != "") {
      this.props.getChicos(padre);
      this.props.getChicas(madre);

      this.props.updateChicos(padre);
      this.props.updateChicas(madre);
    } else {
      this.props.nombresIncomplete();
      error = I18n.t("nombres.error");
    }
  }

  renderList() {
    if (this.props.lista.length < 1) {
      return (
        <View style={styles.message}>
          <Text>{I18n.t("nombres.connection")}</Text>
        </View>
      );
    } else {
      var newList = this.props.lista.filter(function(lista) {
        //return (lista.language.localeCompare(version) == 1);
        return lista.language != version;
      });

      //console.log(newList[0]['title'][locale]);

      return newList.map(lista => (
        <TouchableOpacity
          key={lista["title"][locale]}
          onPress={() => this.onButtonLink(lista.link)}
        >
          <CardList>
            <CardSectionList>
              <View style={styles.container}>
                <View style={styles.thumbnailContainerStyle}>
                  <Image
                    style={styles.listImage}
                    source={{ uri: lista.image }}
                  />
                </View>
                <View style={{ flex: 3 }}>
                  <Text
                    numberOfLines={2}
                    adjustsFontSizeToFit={true}
                    style={styles.textTitle}
                  >
                    {lista["title"][locale]}
                  </Text>
                </View>
                <View style={{ flex: 1.3 }}>
                  <Text style={[styles.textTitle, { color: "grey" }]}>Ad</Text>
                </View>
              </View>
            </CardSectionList>
          </CardList>
        </TouchableOpacity>
      ));
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button onPress={this.onButtonPress.bind(this)}>
        {I18n.t("nombres.calculo")}
      </Button>
    );
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "space-between"
        }}
      >
        <View style={styles.bgImageWrapper}>
          <Image
            style={styles.bgImage}
            source={require("../img/bebe_bg.png")}
          />
        </View>

        <View style={styles.title}>
          <Text
            style={{
              fontFamily: "KGAlwaysAGoodTime",
              fontSize: 30,
              alignSelf: "center"
            }}
          >
            {I18n.t("nombres.title")}
          </Text>
          <View style={styles.subtitle}>
            <Text style={styles.subtitleText}>{I18n.t("nombres.lang")}</Text>
            <TouchableOpacity key="open1" onPress={() => this.setModal(true)}>
              <Image
                style={styles.subtitleImage}
                source={require("../img/flag.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity key="open2" onPress={() => this.setModal(true)}>
              <Image
                style={styles.subtitleImage2}
                source={require("../img/arrow-down-b.png")}
              />
            </TouchableOpacity>
          </View>
        </View>

        <Card>
          <CardSection>
            <View style={{ flex: 3 }}>
              <Card>
                <CardSection>
                  <Input
                    placeholder={I18n.t("nombres.padre")}
                    onChangeText={this.onPadreChange.bind(this)}
                    value={this.props.padre}
                  />
                </CardSection>
              </Card>
            </View>
          </CardSection>
          <CardSection>
            <View style={{ flex: 3 }}>
              <Card>
                <CardSection>
                  <Input
                    placeholder={I18n.t("nombres.madre")}
                    onChangeText={this.onMadreChange.bind(this)}
                    value={this.props.madre}
                  />
                </CardSection>
              </Card>
            </View>
          </CardSection>
        </Card>

        <View style={{ flex: 1 }}>
          <Text style={styles.errorTextStyle}>{error}</Text>
        </View>

        <View style={styles.banner}>
          <AdMobBanner
            bannerSize="banner"
            adUnitID="ca-app-pub-3942358180505264/6296085634"
            testDeviceID="EMULATOR"
            didFailToReceiveAdWithError={this.bannerError}
          />
        </View>

        <View style={styles.button}>{this.renderButton()}</View>

        <Modal
          style={{
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            backgroundColor: "rgba(0, 0, 0, 0.2)"
          }}
          visible={this.props.modal}
        >
          <View style={styles.modal}>
            <View style={{ flex: 1 }}>
              <View style={styles.subtitleModal}>
                <View style={{ flex: 3, marginTop: 5, marginLeft: 20 }}>
                  <Text style={styles.more}>{I18n.t("nombres.more")}</Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "flex-end"
                  }}
                >
                  <TouchableOpacity
                    key="close"
                    onPress={() => this.setModal(false)}
                  >
                    <Image
                      style={styles.closeImage}
                      source={require("../img/close.png")}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={{ flex: 7 }}>
              <ScrollView>{this.renderList()}</ScrollView>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "red",
    marginRight: 20,
    marginLeft: 20
  },
  bgImageWrapper: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  bgImage: {
    flex: 1,
    flexWrap: "wrap",
    width: deviceWidth,
    height: deviceHeight,
    resizeMode: "cover"
  },
  title: {
    flex: 2,
    marginTop: 20
  },
  subtitle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  subtitleModal: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center"
  },
  subtitleText: {
    marginRight: 10
  },
  subtitleImage: {
    marginRight: 5
  },
  subtitleImage2: {
    marginRight: 20,
    alignSelf: "center"
  },
  banner: {
    flex: 3,
    justifyContent: "flex-end",
    alignSelf: "center",
    marginBottom: 2
  },
  button: {
    flex: 1,
    justifyContent: "flex-start"
  },
  modal: {
    flex: 1,
    marginTop: 20,
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20,
    backgroundColor: "white",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "black"
  },
  closeImage: {
    marginTop: 10,
    marginRight: 10
  },
  listImage: {
    alignItems: "center",
    justifyContent: "flex-start",
    alignSelf: "flex-start",
    height: 50,
    width: 50,
    borderRadius: 5,
    borderColor: "grey",
    borderWidth: 1
  },
  textTitle: {
    flex: 1,
    color: "black",
    fontSize: 15,
    marginLeft: 10,
    marginRight: 20,
    paddingRight: 20
  },
  thumbnailContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 5,
    flex: 1
  },
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  message: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  more: {
    fontFamily: "KGAlwaysAGoodTime",
    fontSize: 30,
    alignSelf: "flex-start"
  }
};

const mapStateToProps = ({ nombres }) => {
  const { padre, madre, error, loading, modal, lista } = nombres;

  return { padre, madre, error, loading, modal, lista };
};

export default connect(mapStateToProps, {
  padreChanged,
  madreChanged,
  getChicos,
  getChicas,
  nombresIncomplete,
  updateChicos,
  updateChicas,
  setModal,
  updateLista
})(NombresForm);
