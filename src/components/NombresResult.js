import React, { Component } from 'react';
import { Text, View, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { randomChico, randomChica, originalNames } from '../actions';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { AdMobBanner, AdMobInterstitial, PublisherBanner} from 'react-native-admob'
import { I18n } from 'react-redux-i18n';

var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;

class NombresResult extends Component {
  componentWillMount() {
    this.props.originalNames(this.props.nombres.madre, this.props.nombres.padre, this.props.nombres.chicos, this.props.nombres.chicas);
  }

  onButtonPress() {
  	this.props.randomChico(this.props.nombres.chicos);
  	this.props.randomChica(this.props.nombres.chicas);
  }

  render() {
    return (
       <View style={{ 
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
      }}>
        <View style={{
        	position: 'absolute',
    		top: 0, bottom: 0, left: 0, right: 0
        }}>
          <Image
            style={styles.bgImage}
            source={require('../img/bebe_bg.png')}
          />
        </View>
        <View style={{ height: 30 }}>
        </View>
        <View style={{ flex: 2 }}>
        	<Card>
        		<Text style={styles.captionTextChico}>{I18n.t('nombres.chico')}</Text>
      			<Text style={styles.TextNombre}>{this.props.nombres.randomChico}</Text>
      		</Card>	
      	</View>
      	<View style={{ flex: 2 }}>
      		<Card>	
      			<Text style={styles.captionTextChica}>{I18n.t('nombres.chica')}</Text>
      			<Text style={styles.TextNombre}>{this.props.nombres.randomChica}</Text>
      		</Card>	
      	</View>

        <View style={styles.banner}> 
          <AdMobBanner
            bannerSize="banner"
            adUnitID="ca-app-pub-3942358180505264/6296085634"
            testDeviceID="EMULATOR"
            didFailToReceiveAdWithError={this.bannerError} />
        </View>

      	<View style={styles.button}>
      		<Button onPress={this.onButtonPress.bind(this)}>
	        {I18n.t('nombres.random')}
	      </Button>
      	</View>
        

      </View>	
    );
  }

}

const styles = {
	captionTextChico: {
		color: '#007aff',
    	fontSize: 18,
   	 	fontWeight: '600',
    	paddingTop: 10,
      paddingLeft: 10
	},
	captionTextChica: {
		color: '#ff1493',
    	fontSize: 18,
   	 	fontWeight: '600',
    	paddingTop: 10,
      paddingLeft: 10
	},
	TextNombre: {
		color: 'black',
    	fontSize: 40,
   	 	fontWeight: '600',
    	paddingTop: 10,
    	alignItems: 'center',
    	justifyContent: 'center',
    	alignSelf: 'center'
	},
  bgImage: {
    flex: 1,
    flexWrap: 'wrap',
    width: deviceWidth,
    height: deviceHeight,
    resizeMode: "cover"
  },
  banner: {
    flex: 1, 
    justifyContent: 'flex-end', 
    alignSelf: 'center',
    marginBottom: 2
  },
  button: {
    flex: 2,
    justifyContent: 'flex-start'
  }
}

const mapStateToProps = state => {
  const nombres = state.nombres;

  return { nombres };
};

export default connect(mapStateToProps, { randomChico, randomChica, originalNames })(NombresResult);
