import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import NombresForm from './components/NombresForm';
import NombresResult from './components/NombresResult';

const RouterComponent = () => {
  return (
    <Router 
      sceneStyle={styles.sceneStyle} 
      navigationBarStyle={styles.navBar} 
      titleStyle={styles.navTitle}
      hideNavBar={true}
    >
      <Scene key="main">
        <Scene key="nombres" component={NombresForm} title="NOMBRES DE BEBÉ" />
        <Scene key="result" component={NombresResult} title="RESULTADO" />
      </Scene>
    </Router>
  );
};

const styles = {
  sceneStyle: {
    paddingTop: 0
  },
  navBar: {
    //flex: 1,
    //flexDirection: 'row',
    //alignItems: 'center',
    //justifyContent: 'center',
    backgroundColor: 'rgba(245,209,195,1)', // changing navbar color
  },
  navTitle: {
    color: 'black', // changing navbar title color
    fontWeight: 'bold'
  }

};

export default RouterComponent;