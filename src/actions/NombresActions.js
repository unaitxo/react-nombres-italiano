//import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import {
	DATA_ENTERED,
	PADRE_CHANGED,
	MADRE_CHANGED,
	CHICOS_FETCH_SUCCESS,
	CHICAS_FETCH_SUCCESS,
	RANDOM_CHICO,
	RANDOM_CHICA,
	LOADING_NOMBRES,
  NOMBRES_INCOMPLETE,
  CHICOS_UPDATED,
  CHICAS_UPDATED,
  ORIGINAL_NAMES,
  SET_MODAL,
  UPDATE_LISTA
} from './types';

export const setModal = (modal) => {
  return {
    type: SET_MODAL,
    payload: modal
  };
};

export const updateLista = (lista) => {
  return {
    type: UPDATE_LISTA,
    payload: lista
  };
};

export const madreChanged = (text) => {
  return {
    type: MADRE_CHANGED,
    payload: text
  };
};

export const padreChanged = (text) => {
  return {
    type: PADRE_CHANGED,
    payload: text
  };
};

export const nombresIncomplete = () => {
  return {
    type: NOMBRES_INCOMPLETE,
    payload: 1
  };
};

export const getChicos = (padre) => {
  return (dispatch) => {

  	/*dispatch({ type: LOADING_NOMBRES });

    firebase.database().ref(`/chicos`).orderByChild("num").startAt(5).endAt(99999999999999)
      .on('value', (snapshot) => {
  	      nombreChicoResult(dispatch, snapshot.val());
      });*/

    var chicos = require('../chicos.json');  
    nombreChicoResult(dispatch, chicos.chicos);

  };
};

export const getChicas = (madre) => {
  return (dispatch) => {

    /*firebase.database().ref(`/chicas`).orderByChild("num").startAt(5).endAt(99999999999999)
      .on('value', (snapshot) => {
  	      nombreChicaResult(dispatch, snapshot.val());
      });*/

    var chicas = require('../chicas.json');  
    nombreChicaResult(dispatch, chicas.chicas);

  };
};

export const updateChicos = (padre) => {
  padre = toTitleCase(padre.trim());
  return (dispatch) => {
    /*var fbRef = firebase.database().ref(`/chicos`).orderByChild("nombre").equalTo(padre);

      fbRef.once('value', function(snapshot) {
        if(snapshot.val()){
          writeUpdateChico(dispatch, padre, Object.keys(snapshot.val())[0]);
        } 
        else{
          console.log("El nombre no existe");
          addNewChico(dispatch, padre);
        }
      }, function(error) {
        // The callback failed.
        console.error(error);
      });*/

      dispatch({ type: CHICOS_UPDATED, payload: true });
  };
};

export const updateChicas = (madre) => {
  madre = toTitleCase(madre.trim());
  return (dispatch) => {
    /*var fbRef = firebase.database().ref(`/chicas`).orderByChild("nombre").equalTo(madre);

      fbRef.once('value', function(snapshot) {
        if(snapshot.val()){
          writeUpdateChica(dispatch, madre, Object.keys(snapshot.val())[0]);
        } 
        else{
          console.log("El nombre no existe");
          addNewChica(dispatch, madre);
        }
      }, function(error) {
        // The callback failed.
        console.error(error);
      });*/

      dispatch({ type: CHICAS_UPDATED, payload: true });
  };
};

/*const writeUpdateChico = (dispatch, padre, index) => {
  firebase.database().ref(`/chicos`).child(index)
  .transaction(function(num) {
    // If users/ada/rank has never been set, currentRank will be `null`.
    //return num + 1;
    var number = num.num + 1;
    console.log(number);
    return { nombre: padre, num: number }; 
  }, function(error, committed, snapshot) {
    if (error) {
      console.log('Transaction failed abnormally!', error);
    } else if (!committed) {
      console.log('We aborted the transaction (because ada already exists).');
    } else {
      console.log('User updated!');
    }
    console.log("User's data: ", snapshot.val());
    dispatch({ type: CHICOS_UPDATED, payload: true });
  });
};

const writeUpdateChica = (dispatch, madre, index) => {
  firebase.database().ref(`/chicas`).child(index)
  .transaction(function(num) {
    // If users/ada/rank has never been set, currentRank will be `null`.
    //return num + 1;
    var number = num.num + 1;
    console.log(number);
    return { nombre: madre, num: number }; 
  }, function(error, committed, snapshot) {
    if (error) {
      console.log('Transaction failed abnormally!', error);
    } else if (!committed) {
      console.log('We aborted the transaction (because ada already exists).');
    } else {
      console.log('User updated!');
    }
    console.log("User's data: ", snapshot.val());
    dispatch({ type: CHICAS_UPDATED, payload: true });
  });
};

const addNewChico = (dispatch, padre) => {
  firebase.database().ref(`/chicos`)
  .push().set({ nombre: padre, num: 1 });
  dispatch({ type: CHICOS_UPDATED, payload: true });
};

const addNewChica = (dispatch, madre) => {
  firebase.database().ref(`/chicas`)
  .push().set({ nombre: madre, num: 1 });
  dispatch({ type: CHICAS_UPDATED, payload: true });
};
*/

const nombreChicoResult = (dispatch, nombres) => {
  dispatch({
    type: CHICOS_FETCH_SUCCESS,
    payload: nombres
  });

  //Actions.result();
};

const nombreChicaResult = (dispatch, nombres) => {
  //console.log(nombres);
  dispatch({
    type: CHICAS_FETCH_SUCCESS,
    payload: nombres
  });

  Actions.result();
};

export const randomChico = (nombres) => {
  return (dispatch) => {

    var indices = [];

    Object.keys(nombres).forEach(function(indice) {
      indices.push(indice);
    });

    var randNum = Math.floor((Math.random() * (indices.length)));
    
    console.log("Random: " + randNum);
    console.log(indices[randNum]);

	  dispatch({ type: RANDOM_CHICO, payload: nombres[indices[randNum]].nombre });    

  };
};

export const randomChica = (nombres) => {
  return (dispatch) => {

    var indices = [];

    Object.keys(nombres).forEach(function(indice) {
      indices.push(indice);
    });

    var randNum = Math.floor((Math.random() * (indices.length)));

	  dispatch({ type: RANDOM_CHICA, payload: nombres[indices[randNum]].nombre });    

  };
};

export const originalNames = (madre, padre, nombres_chicos, nombres_chicas) => {
  return (dispatch) => {
    var madre_number = getAsciiNumber(madre);
    var padre_number = getAsciiNumber(padre);
    var parents_number = madre_number + padre_number;
    var nombre_chico = "";
    var nombre_chica = "";
    var nombre_number = 0;
    var diff = 10000000000000;
    var tmp = 0;


    console.log("parents: " + parents_number);

    Object.keys(nombres_chicos).forEach(function(indice) {
      nombre_number = getAsciiNumber(nombres_chicos[indice].nombre);
      nombre_number = nombre_number * 2;
      tmp = Math.abs(parents_number - nombre_number);
      if (tmp < diff){
        nombre_chico = nombres_chicos[indice].nombre;
        diff = tmp;
      }
    });

    diff = 10000000000000;

    Object.keys(nombres_chicas).forEach(function(indice) {
      nombre_number = getAsciiNumber(nombres_chicas[indice].nombre);
      nombre_number = nombre_number * 2;
      tmp = Math.abs(parents_number - nombre_number);
      if (tmp < diff){
        nombre_chica = nombres_chicas[indice].nombre;
        diff = tmp;
      }
    });


    dispatch({ type: ORIGINAL_NAMES, payload: nombre_chica, payload2: nombre_chico });    

  };
};

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function getAsciiNumber(str)
{
  var ascii = 0;
  for(var i = 0; i < str.length; i++)
  {
     ascii = ascii + str.charAt(i).charCodeAt(0);
  }

  return ascii;
}