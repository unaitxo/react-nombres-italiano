import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
//import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import { loadTranslations, setLocale, syncTranslationWithStore } from 'react-redux-i18n';
import reducers from './reducers';
import Router from './Router';
import DeviceInfo from 'react-native-device-info';

class App extends Component {
	componentWillMount() {
		/*const config = {
		  apiKey: 'AIzaSyDsCynt1S8m4aez5PdlEs3J0nFnptuJW6s',
	      authDomain: 'nombres-12029.firebaseapp.com',
	      databaseURL: 'https://nombres-12029.firebaseio.com',
	      storageBucket: 'nombres-12029.appspot.com',
	      messagingSenderId: '992947837647'
		};

		firebase.initializeApp(config);*/

		console.log("Device Locale", DeviceInfo.getDeviceLocale());

	}

	render() {

		const translationsObject = {
			  es:
			   	{
					nombres: {
			      		title: "NOMBRES PARA BEBÉS",
			      		padre: "Nombre del padre",
			      		madre: "Nombre de la madre",
			      		lang: "en italiano",
			      		calculo: "Calcular posibles nombres del bebé",
			      		error: "Hay que introducir el nombre del padre y de la madre",
			      		chico: "Nombre para chico:",
			      		chica: "Nombre para chica:",
			      		random: "Nombres Aleatorios",
			      		more: "Más Nombres",
			      		connection: "Sin conexión a los datos, prueba más tarde por favor."
			    	}
			    },
			en:
			   	{
					nombres: {
			      		title: "NAMES FOR BABIES",
			      		padre: "Father's name",
			      		madre: "Mother's name",
			      		lang: "in Italian",
			      		calculo: "Get names for the baby",
			      		error: "Enter father's and mother's names please",
			      		chico: "Boy name:",
			      		chica: "Girl name:",
			      		random: "Random Names",
			      		more: "More Names",
			      		connection: "No connection to data, try again later please."
			    	}
			    },
			eu:
			  	{
			  		nombres: {
			      		title: "HAURREN IZENAK",
			      		padre: "Aitaren izena",
			      		madre: "Amaren izena",
			      		lang: "italieraz",
			      		calculo: "Haurren izena kalkulatu",
			      		error: "Aitaren eta amaren izena sartu behar da",
			      		chico: "Mutil izena:",
			      		chica: "Neska izena:",
			      		random: "Izen Aleatorioak",
			      		more: "Izen Gehiago",
			      		connection: "Ez dago konexiorik, mesedez probatu berriro beranduago."
			    	}
			  	},
			de:
			  	{
			  		nombres: {
			      		title: "BABY-NAMEN",
			      		padre: "Name des Vaters:",
			      		madre: "Name der Mutter",
			      		lang: "in Italienisch",
			      		calculo: "Berechnen möglich Babynamen",
			      		error: "Sie müssen den Namen des Vaters und der Mutter betreten",
			      		chico: "Jungen nennen:",
			      		chica: "Mädchen Name:",
			      		random: "Zufällige Namen",
			      		more: "Mehr Namen",
			      		connection: "Später Offline-Daten, versuchen Sie es."
			    	}
			  	},
			zh:
			  	{
			  		nombres: {
			      		title: "婴儿名字",
			      		padre: "父亲的名字",
			      		madre: "母亲的名字",
			      		lang: "意大利",
			      		calculo: "计算可能的婴儿名字",
			      		error: "您必须输入父亲和母亲的名字",
			      		chico: "男孩的名字：",
			      		chica: "女孩的名字：",
			      		random: "随机的名字",
			      		more: "更多的名字",
			      		connection: "离线数据，请稍后再试。"
			    	}
			  	},
			cs:
			  	{
			  		nombres: {
			      		title: "DěTSKá JMéNA",
			      		padre: "Jméno otce",
			      		madre: "Jméno matky",
			      		lang: "v italštině",
			      		calculo: "Vypočítat případné dětská jména",
			      		error: "Musíte zadat jméno otce a matky",
			      		chico: "Boys name:",
			      		chica: "Název Girl:",
			      		random: "Náhodné jména",
			      		more: "Další názvy",
			      		connection: "Offline dat, zkuste to prosím později."
			    	}
			  	},
			nl:
			  	{
			  		nombres: {
			      		title: "NAMEN VAN DE BABY",
			      		padre: "Naam van de vader",
			      		madre: "Mother's Naam",
			      		lang: "in het Italiaans",
			      		calculo: "Bereken mogelijk babynamen",
			      		error: "U moet de naam van de vader en moeder in te voeren",
			      		chico: "Boys noemen:",
			      		chica: "Naam van het meisje:",
			      		random: "Random namen",
			      		more: "Meer namen",
			      		connection: "Offline data, probeer het later."
			    	}
			  	},
			fr:
			  	{
			  		nombres: {
			      		title: "BABY NAMES",
			      		padre: "Nom du père",
			      		madre: "Nom de la mère",
			      		lang: "en italien",
			      		calculo: "Calculer les noms pour bébés",
			      		error: "Vous devez entrer le nom du père et de la mère",
			      		chico: "Nom de garçon:",
			      		chica: "Nom de fille:",
			      		random: "Des noms aléatoires",
			      		more: "Plus de noms",
			      		connection: "Données hors ligne, s'il vous plaît essayer plus tard."
			    	}
			  	},
			it:
			  	{
			  		nombres: {
			      		title: "NOMI ITALIANI",
			      		padre: "Il nome del padre",
			      		madre: "Nome della mamma",
			      		lang: "in italiano",
			      		calculo: "Calcolare i possibili nomi del bambino",
			      		error: "È necessario immettere il nome del padre e della madre",
			      		chico: "Ragazzi nome:",
			      		chica: "Ragazza nome:",
			      		random: "Nomi casuali",
			      		more: "Altri nomi",
			      		connection: "I dati non in linea, si prega di riprovare più tardi."
			    	}
			  	},
			ja:
			  	{
			  		nombres: {
			      		title: "赤ちゃんの名前",
			      		padre: "父の名前",
			      		madre: "母の名前",
			      		lang: "イタリア語で",
			      		calculo: "可能性の赤ちゃんの名前を計算します",
			      		error: "あなたは父と母の名前を入力する必要があります",
			      		chico: "男の子の名前：",
			      		chica: "少女の名：",
			      		random: "ランダムな名前",
			      		more: "複数の名前",
			      		connection: "オフラインデータは、後でお試しください。"
			    	}
			  	},
			ko:
			  	{
			  		nombres: {
			      		title: "아기 이름",
			      		padre: "아버지의 이름",
			      		madre: "어머니의 이름",
			      		lang: "이탈리아어",
			      		calculo: "가능한 아기 이름을 계산",
			      		error: "당신은 아버지와 어머니의 이름을 입력해야합니다",
			      		chico: "소년의 이름 :",
			      		chica: "소녀 이름 :",
			      		random: "임의의 이름",
			      		more: "더 이름",
			      		connection: "오프라인 데이터는 나중에 다시 시도하십시오."
			    	}
			  	},
			pl:
			  	{
			  		nombres: {
			      		title: "IMIONA DLA DZIECI",
			      		padre: "Imię ojca",
			      		madre: "Imię matki",
			      		lang: "w języku włoskim",
			      		calculo: "Oblicz możliwe nazwiska dziecka",
			      		error: "Musisz podać nazwisko ojca i matki",
			      		chico: "Chłopcy Nazwa:",
			      		chica: "Nazwa Dziewczyna:",
			      		random: "Losowe nazwy",
			      		more: "Więcej nazwisk",
			      		connection: "Danych w trybie offline, prosimy spróbować później."
			    	}
			  	},
			ru:
			  	{
			  		nombres: {
			      		title: "Детские имена",
			      		padre: "Имя отца",
			      		madre: "Имя матери",
			      		lang: "на итальянском языке",
			      		calculo: "Рассчитайте возможные имена детей",
			      		error: "Вы должны ввести имя отца и матери",
			      		chico: "Мальчики имя:",
			      		chica: "Имя девушки:",
			      		random: "Случайные имена",
			      		more: "больше имен",
			      		connection: "Offline данные, пожалуйста, попробуйте позже."
			    	}
			  	},
			ar:
			  	{
			  		nombres: {
			      		title: "أسماء الطفل",
			      		padre: "اسم الأب",
			      		madre: "اسم الأم",
			      		lang: "باللغة الإيطالية",
			      		calculo: "حساب أسماء الطفل الممكنة",
			      		error: "يجب إدخال اسم الأب والأم",
			      		chico: "بنين أسم:",
			      		chica: "اسم الفتاة:",
			      		random: "أسماء عشوائية",
			      		more: "المزيد من أسماء",
			      		connection: "البيانات حاليا، يرجى المحاولة في وقت لاحق."
			    	}
			  	},
			bg:
			  	{
			  		nombres: {
			      		title: "имена бебе",
			      		padre: "Име на бащата",
			      		madre: "Името на майката",
			      		lang: "в италианския",
			      		calculo: "Изчислете възможни имена на бебето",
			      		error: "Трябва да въведете името на бащата и майката",
			      		chico: "Boys име:",
			      		chica: "Име на момиче:",
			      		random: "Случайни имена",
			      		more: "повече имена",
			      		connection: "Жена данни, моля опитайте по-късно."
			    	}
			  	},
			ca:
			  	{
			  		nombres: {
			      		title: "NOMS PER A NADONS",
			      		padre: "Nom del pare",
			      		madre: "Nom de la mare",
			      		lang: "en italià",
			      		calculo: "Calcular possibles noms del nadó",
			      		error: "Cal introduir el nom del pare i de la mare",
			      		chico: "Nom per a noi:",
			      		chica: "Nom per a noia:",
			      		random: "Noms Aleatoris",
			      		more: "Més Noms",
			      		connection: "Sense connexió a les dades, prova més tard per favor."
			    	}
			  	},
			hr:
			  	{
			  		nombres: {
			      		title: "BABY IMENA",
			      		padre: "Ime oca",
			      		madre: "Naziv Majčin",
			      		lang: "na talijanskom jeziku",
			      		calculo: "Izračunajte mogućih imena za bebe",
			      		error: "Morate unijeti ime oca i majke",
			      		chico: "Dječaci ime:",
			      		chica: "Naziv djevojka:",
			      		random: "Random imena",
			      		more: "Više imena",
			      		connection: "Offline podataka, molimo pokušajte kasnije."
			    	}
			  	},
			da:
			  	{
			  		nombres: {
			      		title: "BABY NAVNE",
			      		padre: "Faderens navn",
			      		madre: "Mors navn",
			      		lang: "i italiensk",
			      		calculo: "Beregn mulige baby navne",
			      		error: "Du skal indtaste navnet på far og mor",
			      		chico: "Drenge navn:",
			      		chica: "Pige navn:",
			      		random: "Tilfældige navne",
			      		more: "Flere navne",
			      		connection: "Offline data, prøv venligst senere."
			    	}
			  	},
			fi:
			  	{
			  		nombres: {
			      		title: "BABY NAMES",
			      		padre: "Isän nimi",
			      		madre: "Äidin nimi",
			      		lang: "italian",
			      		calculo: "Laske mahdolliset vauvan nimiä",
			      		error: "Sinun täytyy antaa nimi isän ja äidin",
			      		chico: "Boys nimi:",
			      		chica: "Girl nimi:",
			      		random: "Random nimet",
			      		more: "Enemmän Names",
			      		connection: "Offline data, yritä myöhemmin."
			    	}
			  	},
			el:
			  	{
			  		nombres: {
			      		title: "Ονόματα μωρών",
			      		padre: "όνομα πατρός",
			      		madre: "Όνομα Μητέρας",
			      		lang: "στα ιταλικά",
			      		calculo: "Υπολογίστε τα πιθανά ονόματα μωρών",
			      		error: "Θα πρέπει να εισάγετε το όνομα του πατέρα και της μητέρας",
			      		chico: "Αγόρια αναφέρουμε:",
			      		chica: "Το όνομά κορίτσι:",
			      		random: "τυχαία ονόματα",
			      		more: "περισσότερα ονόματα",
			      		connection: "Offline δεδομένα, παρακαλούμε δοκιμάστε αργότερα."
			    	}
			  	},
			iw:
			  	{
			  		nombres: {
			      		title: "שמות לתינוקות",
			      		padre: "שם האב",
			      		madre: "שם של אמא",
			      		lang: "באיטלקית",
			      		calculo: "חישוב שמות תינוק אפשריים",
			      		error: "עליך להזין את שם האבא והאמא",
			      		chico: "בנים שם:",
			      		chica: "שם הנערה:",
			      		random: "שמות אקראיים",
			      		more: "יותר שמות",
			      		connection: "נתונים לא מקוונים, אנא נסו מאוחר יותר."
			    	}
			  	},
			hi:
			  	{
			  		nombres: {
			      		title: "बच्चे के नाम",
			      		padre: "पिता का नाम",
			      		madre: "माता का नाम",
			      		lang: "इतालवी",
			      		calculo: "संभव बच्चे के नाम की गणना",
			      		error: "आप पिता और माता का नाम दर्ज करना होगा",
			      		chico: "लड़कों नाम:",
			      		chica: "लड़की का नाम:",
			      		random: "यादृच्छिक नाम",
			      		more: "अधिक नामों",
			      		connection: "ऑफलाइन डेटा, बाद में कोशिश करें।"
			    	}
			  	},
			hu:
			  	{
			  		nombres: {
			      		title: "BABA NEVEK",
			      		padre: "Apja neve",
			      		madre: "Anyja neve",
			      		lang: "olasz",
			      		calculo: "Számítsuk lehetséges baba nevét",
			      		error: "Meg kell adnia a nevét apa és az anya",
			      		chico: "Fiúk neve:",
			      		chica: "Lány neve:",
			      		random: "Véletlen nevek",
			      		more: "További nevek",
			      		connection: "Offline adatok, kérjük, próbálkozzon később."
			    	}
			  	},
			in:
			  	{
			  		nombres: {
			      		title: "NAMA BAYI",
			      		padre: "Nama ayah",
			      		madre: "Nama Ibu",
			      		lang: "di Italia",
			      		calculo: "Hitung nama-nama bayi yang mungkin",
			      		error: "Anda harus memasukkan nama ayah dan ibu",
			      		chico: "Anak laki-laki nama:",
			      		chica: "Nama gadis:",
			      		random: "Nama acak",
			      		more: "Lebih Nama",
			      		connection: "Data offline, coba nanti."
			    	}
			  	},
			lv:
			  	{
			  		nombres: {
			      		title: "BABY NAMES",
			      		padre: "Tēva vārds",
			      		madre: "Mātes vārds",
			      		lang: "itāliešu",
			      		calculo: "Aprēķināt iespējamo bērnu vārdi",
			      		error: "Jums ir jāievada nosaukums tēva un mātes",
			      		chico: "Boys nosaukums:",
			      		chica: "Girl nosaukums:",
			      		random: "Izlases nosaukumi",
			      		more: "Vairāk Names",
			      		connection: "Offline dati, lūdzu, mēģiniet vēlāk."
			    	}
			  	},
			lt:
			  	{
			  		nombres: {
			      		title: "BABY VARDAI",
			      		padre: "Tėvo vardas",
			      		madre: "Motinos vardas",
			      		lang: "italų",
			      		calculo: "Apskaičiuokite galimus kūdikių vardai",
			      		error: "Jūs turite įvesti tėvo ir motinos vardas",
			      		chico: "Berniukai pavadinimas:",
			      		chica: "Mergina pavadinimas:",
			      		random: "Atsitiktinės pavadinimai",
			      		more: "Daugiau Vardai",
			      		connection: "Atsijungęs duomenys, pabandykite vėliau."
			    	}
			  	},
			nb:
			  	{
			  		nombres: {
			      		title: "BABY NAMES",
			      		padre: "Fars navn",
			      		madre: "Mors etternavn",
			      		lang: "på italiensk",
			      		calculo: "Beregn mulige baby navn",
			      		error: "Du må skrive inn navnet på far og mor",
			      		chico: "Gutte navn:",
			      		chica: "Jente navn:",
			      		random: "Tilfeldige navn",
			      		more: "Flere navn",
			      		connection: "Offline data, prøv senere."
			    	}
			  	},
			pt:
			  	{
			  		nombres: {
			      		title: "BABY NAMES",
			      		padre: "Nome do pai",
			      		madre: "Nome da mãe",
			      		lang: "em italiano",
			      		calculo: "Calcule possíveis nomes do bebê",
			      		error: "Você deve digitar o nome do pai e da mãe",
			      		chico: "Meninos Nome:",
			      		chica: "Nome da menina:",
			      		random: "Nomes aleatórios",
			      		more: "Mais nomes",
			      		connection: "Dados off-line, por favor tente mais tarde."
			    	}
			  	},
			ro:
			  	{
			  		nombres: {
			      		title: "NUME COPII",
			      		padre: "Numele tatălui",
			      		madre: "Numele mamei",
			      		lang: "în Italiană",
			      		calculo: "Se calculează posibile nume de copii",
			      		error: "Trebuie să introduceți numele tatălui și al mamei",
			      		chico: "Băieți nume:",
			      		chica: "Nume fată:",
			      		random: "Numele aleatoare",
			      		more: "Mai multe nume",
			      		connection: "Date offline, vă rugăm să încercați mai târziu."
			    	}
			  	},
			sr:
			  	{
			  		nombres: {
			      		title: "Баби Намес",
			      		padre: "Име оца",
			      		madre: "Име мајке",
			      		lang: "ин Италиан",
			      		calculo: "Израчунајте могуће имена за бебе",
			      		error: "Морате унети име оца и мајке",
			      		chico: "Боис наме:",
			      		chica: "Гирл име:",
			      		random: "Рандом имена",
			      		more: "више имена",
			      		connection: "Ван подаци, покушајте касније."
			    	}
			  	},
			sk:
			  	{
			  		nombres: {
			      		title: "DETSKá MENá",
			      		padre: "Meno otca",
			      		madre: "Meno matky",
			      		lang: "v taliančine",
			      		calculo: "Vypočítať prípadné detská mená",
			      		error: "Musíte zadať meno otca a matky",
			      		chico: "Boys name:",
			      		chica: "Názov Girl:",
			      		random: "Náhodné mená",
			      		more: "ďalšie názvy",
			      		connection: "Offline dát, skúste to prosím neskôr."
			    	}
			  	},
			sl:
			  	{
			  		nombres: {
			      		title: "BABY NAMES",
			      		padre: "Ime očeta",
			      		madre: "Materinski Ime",
			      		lang: "v italijanščini",
			      		calculo: "Izračun možnih imen otroka",
			      		error: "Vnesti morate ime očeta in matere",
			      		chico: "Boys ime:",
			      		chica: "Ime dekle:",
			      		random: "Naključne imena",
			      		more: "Več Imena",
			      		connection: "Offline podatkov, poskusite kasneje."
			    	}
			  	},
			sv:
			  	{
			  		nombres: {
			      		title: "BABY NAMES",
			      		padre: "Faderns namn",
			      		madre: "Mors namn",
			      		lang: "i italienska",
			      		calculo: "Beräkna möjliga baby namn",
			      		error: "Du måste ange namnet på far och mor",
			      		chico: "Pojkar namn:",
			      		chica: "Girl namn:",
			      		random: "Slumpmässiga namn",
			      		more: "Fler namn",
			      		connection: "Offlinedata, försök senare."
			    	}
			  	},
			tl:
			  	{
			  		nombres: {
			      		title: "PANGALAN NG SANGGOL",
			      		padre: "Pangalan ng ama",
			      		madre: "Pangalan Ina",
			      		lang: "in Italian",
			      		calculo: "Kalkulahin posibleng mga pangalan ng sanggol",
			      		error: "Kailangan mong ipasok ang pangalan ng ama at ina",
			      		chico: "Boys pangalan:",
			      		chica: "Pangalan Girl:",
			      		random: "Random na mga pangalan",
			      		more: "Mas Pangalan",
			      		connection: "Offline data, mangyaring subukan mamaya."
			    	}
			  	},
			th:
			  	{
			  		nombres: {
			      		title: "ชื่อทารก",
			      		padre: "ชื่อของพระบิดา",
			      		madre: "ชื่อแม่",
			      		lang: "ในภาษาอิตาลี",
			      		calculo: "คำนวณชื่อทารกที่เป็นไปได้",
			      		error: "คุณต้องป้อนชื่อของพ่อและแม่",
			      		chico: "เด็กชายชื่อ:",
			      		chica: "ชื่อสาว:",
			      		random: "ชื่อแบบสุ่ม",
			      		more: "ชื่ออื่น",
			      		connection: "ข้อมูลแบบออฟไลน์โปรดลองใหม่ภายหลัง"
			    	}
			  	},
			tr:
			  	{
			  		nombres: {
			      		title: "BEBEK İSIMLERI",
			      		padre: "Baba Adı",
			      		madre: "Anne Adı",
			      		lang: "İtalyanca",
			      		calculo: "Mümkün bebek isimleri hesaplayın",
			      		error: "Sen anne ve baba adını girmelisiniz",
			      		chico: "Boys adı:",
			      		chica: "Kız ismi:",
			      		random: "Rastgele isimler",
			      		more: "Daha fazla İsimler",
			      		connection: "Çevrimdışı veri, lütfen daha sonra deneyin."
			    	}
			  	},
			uk:
			  	{
			  		nombres: {
			      		title: "Дитячі імена",
			      		padre: "ім'я батька",
			      		madre: "ім'я матері",
			      		lang: "на італійській мові",
			      		calculo: "Розрахуйте можливі імена дітей",
			      		error: "Ви повинні ввести ім'я батька і матері",
			      		chico: "Хлопчики ім'я:",
			      		chica: "Ім'я дівчини:",
			      		random: "випадкові імена",
			      		more: "більше імен",
			      		connection: "Offline дані, будь ласка, спробуйте пізніше."
			    	}
			  	},
			vi:
			  	{
			  		nombres: {
			      		title: "TêN Bé",
			      		padre: "Tên của Cha",
			      		madre: "Tên Mẹ",
			      		lang: "tiếng Ý",
			      		calculo: "Tính tên con tốt",
			      		error: "Bạn phải nhập tên của cha và mẹ",
			      		chico: "Chàng trai tên:",
			      		chica: "Tên cô gái:",
			      		random: "Cái tên ngẫu nhiên",
			      		more: "Nhiều Tên",
			      		connection: "Dữ liệu offline, xin vui lòng thử lại sau."
			    	}
			  	}	
		};

		const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

		syncTranslationWithStore(store);
		store.dispatch(loadTranslations(translationsObject));
		store.dispatch(setLocale(DeviceInfo.getDeviceLocale()));
		this.store = store;

		return (
			<Provider store={store}>
				<Router />
			</Provider>
		);
	}
}

export default App;